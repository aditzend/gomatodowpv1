<?php
/**
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'wp-gomatodo');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'root');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'kZQlZ0UL^,L3n3DL9TVj_2P18L]X-AHK*lWZU31lv^+K&/9f{A#b(YE0A!#R;cP(');
define('SECURE_AUTH_KEY', '9w{`^y1#Wp/:WB<,}tA:5#;y+CH1.Som#RsFbM3KOU8|*{iqIcc##1tXmq3%s6UD');
define('LOGGED_IN_KEY', 'Gy]ERsy}XL@~jhlz}0TUg ~A?`4aQ/c[=;>,?JhX$q$ 8QSd#kc[2aFmJheZLXk ');
define('NONCE_KEY', 'rV+4@WM_{tsi-(58b:sh5^qaxtKjaH}.eM]12w]H_gEa4*j#M]VmwLD4F1TdO2l(');
define('AUTH_SALT', '}G:r5K7lz+Y$ni>f+a.v9[$lzaf]XV@Ok(jX*4nN&7Op7SwAq2{JN$weYT*K[Iz?');
define('SECURE_AUTH_SALT', 'CWHP:+DY%*y*yEYC%E8zWvtlYb7{NKkw)2aU6hvPv!T#<a$bSUsx{l4lw@1L;k8k');
define('LOGGED_IN_SALT', '!ks%~.(Bnmr.#X2DYD;Ocv:CdINW#)7CnGN2MYXXSN!I}sSSb9o*@z$IKyc$rlba');
define('NONCE_SALT', '[zL1-BZl6HI;*z9c)s5{lfbo1~`Rwsdof=+GU 0g@(QXHf5[(Cxj[kNO`A5&_K((');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'gt_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);
define('WP_MEMORY_LIMIT', '2048M');

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
