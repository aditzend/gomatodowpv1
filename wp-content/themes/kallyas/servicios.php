
<!-- Grid icon boxes element section with white background and custom paddings -->
<section id="servicios" class="hg_section--relative bg-white ptop-65 pbottom-0">
  <!-- Background source -->
  <div class="kl-bg-source">
    <!-- Gradient overlay -->
    <div class="kl-bg-source__overlay" style="background: rgba(255,255,255,1); background: -moz-linear-gradient(top,  rgba(255,255,255,1) 0%, rgba(255,255,255,0) 100%); background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,255,255,1)), color-stop(100%,rgba(255,255,255,0))); background: -webkit-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(255,255,255,0) 100%); background: -o-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(255,255,255,0) 100%); background: -ms-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(255,255,255,0) 100%); background: linear-gradient(to bottom,  rgba(255,255,255,1) 0%,rgba(255,255,255,0) 100%); ">
    </div>
    <!--/ Gradient overlay -->
  </div>
  <!--/ Background source -->

  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <!-- Title element with custom bottom padding -->
        <div class="kl-title-block clearfix tbk--text-default tbk--center text-center tbk-symbol--line tbk-icon-pos--after-title pbottom-15">
          <!-- Title with custom montserrat font, size and theme color -->
          <h3 class="tbk__title montserrat fs-20 tcolor">UN CENTRO INTEGRAL DE SERVICIOS</h3>
        </div>
        <!--/ Title element with custom bottom padding -->
        <!-- Grid icon boxes element -->
        <div class="grid-ibx grid-ibx--cols-5 grid-ibx--style-lined-gradient grid-ibx--hover-shadow">
          <div class="grid-ibx__inner">
            <div class="grid-ibx__row clearfix">
              <!-- Item - height 260px -->
              <div class="grid-ibx__item h-260">
                <!-- icon box helper -->
                <i class="grid-ibx__ghelper"></i>
                <!--/ icon box helper -->
                <a href="/gomatodo/tag/neumaticos">
                  <div class="grid-ibx__item-inner">
                    <!-- Title -->
                    <div class="grid-ibx__title-wrp">
                      <h4 class="grid-ibx__title montserrat fs-16">NEUMATICOS</h4>
                    </div>
                    <!--/ Title -->
                      <i class="sprite-jpg sprite-neumaticos"></i>


                    <!-- Content -->
                    <div class="grid-ibx__desc-wrp">
                      <!-- Description -->
                      <p class="grid-ibx__desc">
                        Compr&aacute; tus neum&aacute;ticos en un distribuidor oficial.
                      </p>

                      <!--/ Description -->
                    </div>
                    <a href="/gomatodo/tag/neumaticos" type="button" class="btn btn-element btn-lined lined-gray btn-sm">
                      VER
                    </a >

                    <!--/ Content -->
                  </div>
                  <!--/ .grid-ibx__item-inner -->
                </a>
              </div>
              <!--/ Item - height 260px - .grid-ibx__item -->
              <a href="/gomatodo/tag/alineacion">
              <!-- Item - height 260px -->
              <div class="grid-ibx__item h-260">
                <!-- icon box helper -->
                <i class="grid-ibx__ghelper"></i>
                <!--/ icon box helper -->

                <div class="grid-ibx__item-inner">
                  <!-- Title -->
                  <div class="grid-ibx__title-wrp">
                    <h4 class="grid-ibx__title montserrat fs-16">ALINEACION</h4>
                  </div>
                  <!--/ Title -->
                  <i class="sprite-jpg sprite-alineacion"></i>



                  <!-- Content -->
                  <div class="grid-ibx__desc-wrp">
                    <!-- Description -->
                    <p class="grid-ibx__desc">
                      Contamos con la m&aacute;s alta tecnolog&iacute;a HUNTER 3D.
                    </p>
                    <!--/ Description -->
                  </div>
                  <button type="button" class="btn btn-element btn-lined lined-gray btn-sm">
                    VER
                  </button>
                  <!--/ Content -->
                </div>
                <!--/ .grid-ibx__item-inner -->
              </div>
            </a>

              <!--/ Item - height 260px - .grid-ibx__item -->
              <a href="/gomatodo/tag/suspension">
              <!-- Item - height 260px -->
              <div class="grid-ibx__item h-260">
                <!-- icon box helper -->
                <i class="grid-ibx__ghelper"></i>
                <!--/ icon box helper -->

                <div class="grid-ibx__item-inner">
                  <!-- Title -->
                  <div class="grid-ibx__title-wrp">
                    <h4 class="grid-ibx__title montserrat fs-16">SUSPENSION</h4>
                  </div>
                  <!--/ Title -->
                  <i class="sprite-jpg sprite-suspension"></i>



                  <!-- Content -->
                  <div class="grid-ibx__desc-wrp">
                    <!-- Description -->
                    <p class="grid-ibx__desc">
                      Somos el #1 con las mejores marcas de amortiguadores.
                    </p>

                    <!--/ Description -->
                  </div>
                  <button type="button" class="btn btn-element btn-lined lined-gray btn-sm">
                    VER
                  </button>

                  <!--/ Content -->
                </div>
                <!--/ .grid-ibx__item-inner -->
              </div>
              <!--/ Item - height 260px - .grid-ibx__item -->
            </a>
              <a href="/gomatodo/tag/frenos">

              <!-- Item - height 260px -->
              <div class="grid-ibx__item h-260">
                  <!-- icon box helper -->
                  <i class="grid-ibx__ghelper"></i>
                  <!--/ icon box helper -->
                    <div class="grid-ibx__item-inner">
                      <!-- Title -->
                      <div class="grid-ibx__title-wrp">
                        <h4 class="grid-ibx__title montserrat fs-16">FRENOS</h4>
                      </div>
                      <!--/ Title -->
                      <i class="sprite-jpg sprite-frenos"></i>



                      <!-- Content -->
                      <div class="grid-ibx__desc-wrp">
                        <!-- Description -->
                        <p class="grid-ibx__desc">
                          Tenemos el kit de frenos para tu auto. Encontralo ya.
                        </p>

                        <!--/ Description -->
                      </div>
                      <button type="button" class="btn btn-element btn-lined lined-gray btn-sm">
                        VER
                      </button>

                      <!--/ Content -->
                    </div>
                    <!--/ .grid-ibx__item-inner -->
              </div>
            </a>

              <!--/ Item - height 260px - .grid-ibx__item -->
              <a href="/gomatodo/tag/tren">
              <!-- Item - height 260px -->
              <div class="grid-ibx__item h-260">
                <!-- icon box helper -->
                <i class="grid-ibx__ghelper"></i>
                <!--/ icon box helper -->

                <div class="grid-ibx__item-inner">
                  <!-- Title -->
                  <div class="grid-ibx__title-wrp">
                    <h4 class="grid-ibx__title montserrat fs-16">TREN DELANTERO</h4>
                  </div>
                  <!--/ Title -->
                  <i class="sprite-jpg sprite-tren"></i>



                  <!-- Content -->
                  <div class="grid-ibx__desc-wrp">
                    <!-- Description -->
                    <p class="grid-ibx__desc">
                      Control&aacute; y repar&aacute; tu tren delantero.
                    </p>

                    <!--/ Description -->
                  </div>
                  <button type="button" class="btn btn-element btn-lined lined-gray btn-sm">
                    VER
                  </button>

                  <!--/ Content -->
                </div>
                <!--/ .grid-ibx__item-inner -->
              </div>
              <!--/ Item - height 260px - .grid-ibx__item -->
            </a>


            </div>
            <!--/ .grid-ibx__row -->
          </div>
          <!--/ .grid-ibx__inner -->
        </div>
        <!--/ Grid icon boxes element -->

      </div>
      <!--/ col-md-12 col-sm-12 -->
    </div>
    <!--/ row -->
  </div>
  <!--/ container -->
</section>
<!--/ Grid icon boxes element section with white background and custom paddings -->
