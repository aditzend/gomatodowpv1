<?php
/*
Template Name: Chuchi
*/
?>
<?php if(! defined('ABSPATH')){ return; }
/**
 * Template layout for ARCHIVES
 * @package  Kallyas
 * @author   Team Hogash
 */
get_header();


/*** USE THE NEW HEADER FUNCTION **/
	// Saved title in options
	$title = zget_option( 'archive_page_title', 'blog_options' );
	$subtitle = zget_option( 'archive_page_subtitle', 'blog_options' );
	if( empty( $title ) ){
		//** Put the header with title and breadcrumb
		$title = __( 'Blog', 'zn_framework' );
	}

	if ( is_home() && $blog_page_id = get_option( 'page_for_posts' ) ){
		$title = get_the_title( $blog_page_id );
	}
	WpkPageHelper::zn_get_subheader( array( 'title' => $title, 'subtitle' => $subtitle ) );

	// Check to see if the page has a sidebar or not
	$main_class = zn_get_sidebar_class('blog_sidebar');
	if( strpos( $main_class , 'right_sidebar' ) !== false || strpos( $main_class , 'left_sidebar' ) !== false ) { $zn_config['sidebar'] = true; } else { $zn_config['sidebar'] = false; }
	$sidebar_size = zget_option( 'sidebar_size', 'unlimited_sidebars', false, 3 );
	$content_size = 12 - (int)$sidebar_size;
	$zn_config['size'] = $zn_config['sidebar'] ? 'col-sm-8 col-md-'.$content_size : 'col-sm-12';
?>
<section class="site-content">
  <h1>chorizo
    <?php include "mercadooca.html"; ?>
<?php include "servicios.php"; ?>
  </h1>

  <section class="zn_section eluide409d669    zn_ovhidden section-sidemargins    zn_section--masked zn_section--relative section--no " id="eluide409d669">

  			<div class="zn-bgSource "><div class="zn-bgSource-overlayGloss"></div></div>
  			<div class="zn_section_size container zn-section-height--auto zn-section-content_algn--top ">

  				<div class="row ">

  		<div class="eluidf3938a8b      col-md-9 col-sm-9   znColumnElement" id="eluidf3938a8b">


  			<div class="znColumnElement-innerWrapper-eluidf3938a8b znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left ">

  				<div class="znColumnElement-innerContent">					<div class="kl-title-block clearfix tbk--text- tbk--left text-left tbk-symbol--line tbk--colored tbk-icon-pos--before-title eluid3a7216c7 "><span class="tbk__symbol "><span></span></span><h2 class="tbk__title" itemprop="headline">KALLYAS THEME... RELOADED!</h2><h4 class="tbk__subtitle" itemprop="alternativeHeadline">This is the brand new 4.0 series of the Kallyas Theme, resurrected, totally re-shaped and hungry for new awesome features.</h4></div>				</div>
  			</div>


  		</div>

  		<div class="eluide7f419be      col-md-3 col-sm-3   znColumnElement" id="eluide7f419be">


  			<div class="znColumnElement-innerWrapper-eluide7f419be znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left ">

  				<div class="znColumnElement-innerContent">					<div class="th-spacer clearfix eluid69c261d0     "></div><div id="eluid0ac8d52e" class="zn_buttons_element eluid0ac8d52e text-right "><a href="http://themeforest.net/item/kallyas-responsive-multipurpose-wordpress-theme/4091658?license=regular&amp;open_purchase_for_item_id=4091658&amp;purchasable=source&amp;ref=hogash" id="eluid0ac8d52e0" class="eluid0ac8d52e0 btn-element btn-element-0 btn  btn-fullwhite   zn_dummy_value btn-icon--before btn--rounded" target="_blank" itemprop="url"><span>BUY NOW</span></a><a href="#" id="eluid0ac8d52e1" class="eluid0ac8d52e1 btn-element btn-element-1 btn  btn-lined   zn_dummy_value btn-icon--before btn--rounded" target="_self" itemprop="url"><span>LEARN MORE</span></a></div>				</div>
  			</div>


  		</div>

  				</div>
  			</div>

  			<div class="kl-mask kl-bottommask kl-mask--mask3 mask3l kl-mask--light">
  <svg width="5000px" height="57px" class="svgmask svgmask-left" viewBox="0 0 5000 57" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
      <defs>
          <filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-mask3">
              <feOffset dx="0" dy="3" in="SourceAlpha" result="shadowOffsetInner1"></feOffset>
              <feGaussianBlur stdDeviation="2" in="shadowOffsetInner1" result="shadowBlurInner1"></feGaussianBlur>
              <feComposite in="shadowBlurInner1" in2="SourceAlpha" operator="arithmetic" k2="-1" k3="1" result="shadowInnerInner1"></feComposite>
              <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.4 0" in="shadowInnerInner1" type="matrix" result="shadowMatrixInner1"></feColorMatrix>
              <feMerge>
                  <feMergeNode in="SourceGraphic"></feMergeNode>
                  <feMergeNode in="shadowMatrixInner1"></feMergeNode>
              </feMerge>
          </filter>
      </defs>
      <path d="M9.09383679e-13,57.0005249 L9.09383679e-13,34.0075249 L2418,34.0075249 L2434,34.0075249 C2434,34.0075249 2441.89,33.2585249 2448,31.0245249 C2454.11,28.7905249 2479,11.0005249 2479,11.0005249 L2492,2.00052487 C2492,2.00052487 2495.121,-0.0374751261 2500,0.000524873861 C2505.267,-0.0294751261 2508,2.00052487 2508,2.00052487 L2521,11.0005249 C2521,11.0005249 2545.89,28.7905249 2552,31.0245249 C2558.11,33.2585249 2566,34.0075249 2566,34.0075249 L2582,34.0075249 L5000,34.0075249 L5000,57.0005249 L2500,57.0005249 L1148,57.0005249 L9.09383679e-13,57.0005249 Z" class="bmask-bgfill" filter="url(#filter-mask3)" fill="#f5f5f5" style="fill:"></path>
  </svg>
  <i class="glyphicon glyphicon-chevron-down"></i>
  </div>		</section>


</section>
	<section id="content" class="site-content" >
		<div class="container">
			<div class="row">

				<div id="mainbody" class="<?php echo $main_class;?>">
					<?php

					$columns = zget_option( 'blog_style_layout', 'blog_options', false, '1' );
					$blog_layout = $columns > 1 ? 'cols' : 'def_classic';
					$blog_layout = zget_option( 'blog_layout', 'blog_options', false, $blog_layout );

					if ( $blog_layout == 'cols' ) {
						get_template_part( 'blog', 'columns' );
					}
					elseif ( $blog_layout == 'def_classic' || $blog_layout == 'def_modern' ) {
						get_template_part( 'blog', 'default' );
					}
					?>
				</div><!--// #mainbody -->
				<?php get_sidebar(); ?>
			</div><!--// .row -->
		</div><!--// .container -->
	</section><!-- end content -->
<?php get_footer();
